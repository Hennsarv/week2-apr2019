﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VeadProgrammis
{
    class Program
    {
        static void Main(string[] args)
        {

            // jagamise programm
            try // ega ei juhtu miskit valesti
            {
                Console.Write("Anna üks arv: ");
                int yks = int.Parse(Console.ReadLine());
                Console.Write("Anna teine arv: ");
                int teine = int.Parse(Console.ReadLine());

                if (yks < teine) throw new Exception("väiksemat suuremaga põle mõtet jagada - ei jagu");

                int jagatis = yks / teine;
                Console.WriteLine($"nende jagatis on {jagatis}");
            }
            catch (DivideByZeroException e) // kui juhtus midagi valesti
            {
                // Console.WriteLine("midagi läks nihu");
                // Console.WriteLine(e.Message);
                Console.WriteLine("lollakas - nulliga ei saa jagada");
            }
            catch (FormatException e)
            {
                Console.WriteLine("sa ei oska numbreid kirjutada");
            }

            catch (Exception e)
            {
                Console.WriteLine("juhtus midagi muud veidrat");
                Console.WriteLine(e.Message);
            }
    Console.WriteLine("lõpetasin töö");
        }
    }
}
