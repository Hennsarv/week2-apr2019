﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kollektsioonid
{
    static class Program
    {
        //static bool KasPaaris(int x) => x % 2 == 0; 

        static void Main(string[] args)
        {
            // see on array
            var arvudMassiiv = Enumerable.Range(1,100);

            

            

            // see on list
            List<int> arvudList = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 17 };

            arvudMassiiv
                //.Where(x => x % 2 == 0)
                .Skip(3)
                .Take(5)
                //.Select( x => x*x)
                .Select(x => $"{x} kuup on {x*x*x}")
                .Tryki();


            var tulemus = arvudMassiiv
                .Where(X => X % 2 == 0)
                .Take(5)
                .Sum();
            Console.WriteLine(tulemus);

            Console.WriteLine(
                arvudMassiiv
                .Select(x => x*x)
                .Where (x => x < 100)
                .Count()
                );


            Console.WriteLine("\npaaris enne paaritud järel\n");
            arvudList
                .OrderBy(x => x % 2)
                .Tryki();


            #region peitu
            //// emba kumba on võimalik foreachiga läbi käia
            ////Tryki(arvudList);
            //arvudMassiiv
            //    .Millised(x => x < 3)
            //    .Tryki()
            //    ;

            //Func<int, bool> paaris = x => x % 2 == 0;
            //Func<int, bool> paaritu = x => x % 2 == 0;

            //var mis = DateTime.Now.DayOfWeek == DayOfWeek.Wednesday ? paaris : paaritu;
            //arvudList
            //    .Millised(mis)
            //    .TeeMidagi(x => Console.WriteLine(x));
            //    ; 
            #endregion

        }

        // tahaks teha meetodi, mis massiivi või listi välja prindib (nagu ülal)
        static void Tryki<T>(this IEnumerable<T> mida)
        {
            foreach (var x in mida) Console.WriteLine(x);
        }

        static void TeeMidagi<T>(this IEnumerable<T> mass, Action<T> a)
        {
            foreach (var x in mass) a(x);
        }

        static IEnumerable<int> Millised(this IEnumerable<int> mass, Func<int, bool> f)
        {
            foreach (var x in mass)
            {
                if (f(x))
                {
                    yield return x;
                }
            }
        }
        
        //static IEnumerable<int> Skip(this IEnumerable<int> mass, int mitu)
        //{
        //    int i = 0;
        //    foreach(var x in mass)
        //    {
        //        if (++i > mitu) yield return x;

        //    }

        //}


        //static IEnumerable<int> Take(this IEnumerable<int> mass, int mitu)
        //{
        //    int i = 0;
        //    foreach(var x in mass)
        //    {
        //        if (i++ > mitu) break;
        //        yield return x;
        //    }
        //}

    }
}
