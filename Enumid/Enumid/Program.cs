﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enumid
{
    class Program
    {
        static void Main(string[] args)
        {
            Sugu s = Sugu.Mees;
            Console.WriteLine(s);
            Console.WriteLine((int)s);

            Console.WriteLine( DayOfWeek.Monday );

            Mast m = Mast.Pada;
            m--;
            Console.WriteLine(m);

            Kirjeldus k = Kirjeldus.Puust | Kirjeldus.Punane;
            Console.WriteLine(k);
            k |= Kirjeldus.Suur;
            Console.WriteLine(k);
            k ^= Kirjeldus.Punane;
            Console.WriteLine(k);
            k = (Kirjeldus)13;
            Console.WriteLine(k);

        }
    }

    enum Sugu { Naine, Mees}; // nimelised konstandid, mis pandud ühte punti ja tähendavad sama laadi asja

    enum Mast { Risti = 1, Ruutu, Ärtu, Poti, Pada = 4}

    [Flags] enum Kirjeldus { Puust = 1, Punane = 2, Suur = 4, Kolisev = 8}


}
