﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InimeseSugupuu
{
    class Program
    {
        static void Main(string[] args)
        {
            Inimene henn = new Inimene("Henn", Sugu.Mees);
            Inimene maris = new Inimene("Maris", Sugu.Naine);
            Inimene eve = new Inimene("Eve", Sugu.Naine);

            henn.Abielu(eve);
            henn.LisaLaps(new Inimene("Krister", Sugu.Mees));
            henn.LisaLaps(new Inimene("Holger", Sugu.Mees));

            henn.Lahutus();



            Inimene.Abielu(henn, maris);

            Console.WriteLine(henn);
            Console.WriteLine(maris);

            henn.LisaLaps(new Inimene("Mai-Liis", Sugu.Naine));
            henn.LisaLaps(new Inimene("Pille-Riin", Sugu.Naine));
            
            foreach (var x in henn.Lapsed) Console.WriteLine(x);

        }
    }

    public enum Sugu { Naine, Mees }

    class Inimene
    {
        private static Dictionary<string, Inimene> _Inimesed = new Dictionary<string, Inimene>();
        public static IEnumerable<Inimene> Inimesed => _Inimesed.Values;

        public string Nimi { get; set; }
        public Sugu Sugu { get; private set; }

        public Inimene Kaasa { get; private set; } = null;

        public Inimene Ema { get; private set; } = null;
        public Inimene Isa { get; private set; } = null;

        private List<Inimene> _Lapsed = new List<Inimene>();
        public IEnumerable<Inimene> Lapsed => _Lapsed.AsEnumerable();

        public Inimene(string nimi, Sugu sugu)
        {
            Nimi = nimi; Sugu = sugu;
        }

        public void Abielu(Inimene kaasa) => Abielu(this, kaasa);
        //{
        //    Abielu(this, kaasa);
        //}

        public static void Abielu(Inimene kes, Inimene kaasa)
        {
            // alguses paar kontrolli, 1-kes ei ole abielus, 2-kaasa ei ole null, 3-kes ja kaasa pole samast soost
            if (kes.Kaasa != null) throw new Exception($"{kes.ToString()} on juba abielus");
            if (kaasa == null) return;
            if (kes.Sugu == kaasa.Sugu) throw new Exception("meil on samasooline abielu keelatud");

            kes.Kaasa = kaasa;  // abiellujad märgitakse teineteise kaasadeks
            kaasa.Kaasa = kes;
        }

        public void Lahutus()
        {
            if (Kaasa != null) // kui vallaline, pole mõtet midagi teha
            {
                Kaasa.Kaasa = null;    // kaasa märgitakse vallaliseks
                Kaasa = null;          // ja this märgitakse vallaliseks
            }
        }

        public void LisaLaps(Inimene laps)
        {
            if (laps == null) return;  // olematut last me ei lisa
            this._Lapsed.Add(laps);    // paneme this laste hulka kija
            if (Kaasa != null) Kaasa._Lapsed.Add(laps); // kui on kaasa, siis paneme ka tema laste hulka kirja
            if (this.Sugu == Sugu.Mees)
            {
                laps.Isa = this;       // kui this on mees, paneme ta kirja lapsele isaks
                laps.Ema = this.Kaasa; // ja tema kaasa lapsele emaks
            }
            else                       // else teeme vastupidi
            {
                laps.Ema = this;
                laps.Isa = this.Kaasa;
            }

        }

        // tegin tostringi jaoks kaks varianti - ühe if-ga, teise if-ta
        // kommenteeri vabalt üks sisse, teine välja
        public override string ToString()
            /* vahetamiseks lisa selle rea algusse //
           => ToStringIfiga();
           // */ => ToStringIfita();

        public string ToStringIfita() // suht keeruline avaldis ilma ifideta
            => $"{Sugu} {Nimi}"       // algusse paneme inimese soo ja nime
            + (Kaasa == null ? "" : $" (tal on {Kaasa.Sugu} {Kaasa.Nimi})")   // kui on kaasa, lisame tema soo nime
            + ((Ema == null && Isa == null) ? ""   // kui on emb-kumb vanematest lisame sulgudes nende nimed
                : $"({(Isa == null ? "" : $" isa:{Isa.Nimi}") + (Ema == null ? "" : $" ema:{Ema.Nimi}")} ) "
                // kui isa on, lisame isa,                       kui ema on, lisame ema
                )
            ;

        public string ToStringIfiga()  // to stringi variant IFidega
        {
            string vastus = $"{Sugu} {Nimi}";   // algusse paneme inimese soo ja nime

            if (Kaasa != null)
                vastus += $" (tal on {Kaasa.Sugu} {Kaasa.Nimi})"; // kui on kaasa, lisame tema soo ja nime
            if (Ema != null || Isa != null) // kui on emb-kummb isa või ema, siis sulud
            {
                vastus += " ("; // ja sulgudes
                if (Isa != null) vastus += $" isa:{Isa.Nimi}"; // kui isa on, siis isa nimi
                if (Ema != null) vastus += $" isa:{Ema.Nimi}"; // kui ema on, siis ema nimi
                vastus += " )";
            }
            return vastus;
        }
    }
}
