﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MeetodidJaFunktsioonid
{
    class Program
    {
        static void Main(string[] args)
        {
            Inimene i = new Inimene();
            i.PaneNimi("Keegi");
            Console.WriteLine(i.AnnaNimi());
            Console.WriteLine(Inimene.AnnaNimi(i));

            Console.WriteLine(i.GetNimi());
            Console.WriteLine(E.GetNimi(i));

            string xxx = "Mingi string, millega midagi teha";
            Console.WriteLine(xxx.ToLower());

            Console.WriteLine(xxx.ToProper());

            string yyy = xxx
                .ToProper()
                //.Substring(4, 7)
                .Trim()
                ;

            int a = 7; int b = 8;
            var c = Liida(c: 7, a: 4);

            Console.WriteLine(Liida("Henn", 4));


            int x = Liida2(1, 2, 3);
            int x4 = Liida2(2, 7, 8, 1, 0, 3, 7, 1);
            int x5 = Liida2();


            
            Console.WriteLine($"a = {a} ja b = {b}");
            Swap(ref a, ref b);
            Console.WriteLine($"a = {a} ja b = {b}");

            Inimene i1 = new Inimene(); i1.PaneNimi("Henn");
            Inimene i2 = new Inimene(); i2.PaneNimi("Ants");
            Console.WriteLine($"i1 on {i1.AnnaNimi()} i2 on {i2.AnnaNimi()}");
            Swap(ref i1, ref i2);
            Console.WriteLine($"i1 on {i1.AnnaNimi()} i2 on {i2.AnnaNimi()}");



        }

        //static int Liida(int a, int b)
        //{
        //    return a + b;
        //}
        static int Liida(int a = 0, int b = 0, int c = 0)
        {
            return a + b*10 + c*100;
        }

        static string Liida(string a, int b)
        {
            return a + b.ToString();
        }

        static long Liida(long a, int b) => a + b;
        static long Liida(int a, long b) => a + b;

        static int Liida2(params int[] arvud)
        {
            int summa = 0; foreach (var x in arvud) summa += x; return summa;
        }

        public static void Vaheta(ref int a, ref int b)
        {
            var x = a;
            a = b;
            b = x;
        }

        public static void Vaheta(ref string a, ref string b)
        {
            var x = a;
            a = b;
            b = x;
        }

        public static void Vaheta(ref Inimene a, ref Inimene b)
        {
            var x = a;
            a = b;
            b = x;
        }

        public static void Swap<T>(ref T a, ref T b)
        {
            T x = a;
            a = b;
            b = x;


        }




    }

    class Inimene
    {
        string Nimi;

        public void PaneNimi(string nimi) => Nimi = nimi;
        public static void PaneNimi(Inimene x, string nimi) => x.Nimi = nimi;

        public string AnnaNimi() => this.Nimi;
        public static string AnnaNimi(Inimene x) => x.Nimi;

        
    }

    static class E
    {
        public static string GetNimi(this Inimene x) => x.AnnaNimi();

        public static string ToProper(this string tekst)
        {
            if (tekst == "") return "";
            var sõnad = tekst.Split(' ');
            for (int i = 0; i < sõnad.Length; i++)
            {
                sõnad[i] = sõnad[i].Substring(0, 1).ToUpper() + sõnad[i].Substring(1).ToLower();
            }
            return string.Join(" ", sõnad);
        }

    }
}
