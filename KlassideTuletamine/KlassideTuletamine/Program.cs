﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KlassideTuletamine
{
    static class Program
    {
        public static void MyForEach<T>(this IEnumerable<T> m, Action<T> a)
        {
            foreach (var x in m) a(x);
        }


        static void Main(string[] args)
        {
            //Loom prussa = new Koduloom("prussakas") { Nimi = "Albert" };
            Loom kroko1 = new Metsloom { Liik = "krokodill" };

            Koduloom kiisu = new Kass { Nimi = "Miisu" };
            Koduloom kiisu1 = new Kass { Nimi = "Kurri" };

            //foreach (var x in Loom.Loomaaed) x.TeeHäält();


            Console.WriteLine("\nloomaaia katsetus\n");
            Loom.Loomaaed.MyForEach(x => { x.Silita(); x.TeeHäält(); });

            // TODO: siin on miski jama
            Loom.Loomaaed
                .Where(x => x is Kass)
                .Select(x => x as Kass)
                //.OfType<Kass>()
                .MyForEach(x => { x.SikutaSabast(); x.TeeHäält(); });

            Console.WriteLine("\nloomaaia katsetus lõpp\n");


            kroko1.TeeHäält();
            kroko1.SikutaSabast();

            kiisu.Silita();
            kiisu.TeeHäält();
            kiisu.SikutaSabast();
            kiisu.TeeHäält();

            kroko1.Sööb("elevandipoega");

            //Lõuna(new Sepik());
            Lõuna(kroko1);


        }

        public static void Lõuna(object x)
        {

            Console.WriteLine($"Lõunaks anti {x}");
            //if (x is ISöödav)
            //    Console.WriteLine("me saame süüa");
            //else
            //    Console.WriteLine("jääme nälga");

            //ISöödav s = x as ISöödav;
            //if (s == null) Console.WriteLine("nälg majas");
            //else s.Süüakse();

            if (x is ISöödav s) s.Süüakse();
            else Console.WriteLine("jääme nälga");

            var sss = x is ISöödav ? x as ISöödav : null;
            var ss2 = x as ISöödav;
        }


    }

    abstract class Loom
    {
        private static List<Loom> _Loomaaed = new List<Loom>();
        public static IEnumerable<Loom> Loomaaed => _Loomaaed.AsEnumerable();

        public string Liik { get; set; } // = "tundmatu liik";
        public int RegNr;

        // Looma konstruktorid
        public Loom() : this("tundmatu liik") { }
        public Loom(string liik) { Liik = liik; _Loomaaed.Add(this); }
            public Loom(string liik, int regnr) : this(liik) => RegNr = regnr;

        // looma meetodid
        public virtual void Silita() => Console.WriteLine($"ettevaatust {Liik} võib olla ohtlik - ära silita");
        public virtual void TeeHäält() => Console.WriteLine($"{Liik} teeb koledat häält");
        public virtual void SikutaSabast() => Console.WriteLine($"{Liik} paneb su nahka");

        public abstract void Sööb(string midagi); // mida ei ole veel defineeritud

        // Looma tostring
        public override string ToString() => $"numbri {RegNr} all on loom liigist {Liik}";
    }


    abstract class Koduloom : Loom
    {
        static int loendur = 0;
        public string Nimi { get; set; } = "nimetu";

        public Koduloom(string liik) : base(liik, ++loendur) { }

        // kodulooma tostring
        public override string ToString() => $"numbri {RegNr} all on {Liik} {Nimi}";
    }

    class Metsloom : Loom
    {
        public override void Sööb(string midagi)
        {
            Console.WriteLine($"{Liik} õgib {midagi}");
        }
    }

    class Kass : Koduloom
    {
        public string Tõug { get; set; } = "noname";
        private bool _Tuju = false;  // kassil on algselt halb tuju
   
        public Kass() : base("kass") { }

        public override void SikutaSabast() => _Tuju = false;
        public override void Silita() => _Tuju = true;

        public override void TeeHäält()
        {
            if (_Tuju) Console.WriteLine($"kass {Nimi} lööb nurru");
            else Console.WriteLine($"kass {Nimi} kräunub");
        }

        public override void Sööb(string midagi)
        {
            Console.WriteLine($"Kass {Nimi} limpsib {midagi}"); 
        }
    }

    class Koer : Koduloom, ISöödav
    {
        public Koer() : base("koer") { }

        public override void TeeHäält()
        {
            Console.WriteLine($"koer {Nimi} haugub");
            // TODO: koerte haukumine tuleb korda teha

        }

        public override void Sööb(string midagi)
        {
            Console.WriteLine($"Kass {Nimi} närib {midagi}");
        }

        public void Süüakse()
        {
            Console.WriteLine($"koer {Nimi} pannakse nahka"); 
        }
    }

    interface ISöödav
    {
        void Süüakse();
        // TODO: enne söömist pese käsi
    }

    class Sepik : ISöödav
    {
        public void Süüakse()
        {
            Console.WriteLine("keegi nosib sepikut"); 
            // TODO: lisa kontroll, et sepik ei oleks halllitama läinud
        }
    }
}
