﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MängukooliÜlesanne;

namespace Kooliaken
{
    public partial class Õpilased : Form
    {
        public Õpilased()
        {
            InitializeComponent();
            this.Text = "Õpilased või õpetajad";
            this.dataGridView1.Visible = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Inimene.LoeInimesedJson();
            this.dataGridView1.DataSource = Inimene.Õpilased.ToList();
            this.dataGridView1.Columns["Aine"].Visible = false;
            this.Text = "Õpilaste nimekiri";
            this.dataGridView1.Visible = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Inimene.LoeInimesedJson();
            this.dataGridView1.DataSource = Inimene.Õpetajad.ToList();
            this.dataGridView1.Columns["Aine"].Visible = true;
            this.Text = "Õpside nimekiri";
            this.dataGridView1.Visible = true;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Text = "Õpilased või õpetajad";
            this.dataGridView1.DataSource = null;
            this.dataGridView1.Visible = false;

        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }
    }
}
