﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Newtonsoft.Json;

namespace MängukooliÜlesanne
{
    public class KlassAine
    {
        static string folder = @"..\..\..\";


        public static List<KlassAine> KlassAined = new List<KlassAine>();

        public string Klass { get; set; } = "";
        public string Aine { get; set; } = "";

        public KlassAine() => KlassAined.Add(this);

        public static void LoeKlassidAined()
        {
            string ainedFilename = folder + "KlassJaAine.txt";

            File.ReadAllLines(ainedFilename)
             .Select(x => (x + ",,").Split(',').Select(y => y.Trim()).ToArray())
             .ForEach(x => new KlassAine { Aine = x[1], Klass = x[0] });

        }

        public static void KirjutaKlassidAinedJson()
        {
            string ainedFilename = folder + "KlassJaAine.json";

            File.WriteAllText(ainedFilename,
                JsonConvert.SerializeObject(KlassAined)
                );
        }

        public static void LoeKlassidAinedJson()
        {
            string ainedFilename = folder + "KlassJaAine.json";

            KlassAined =
                JsonConvert.DeserializeObject<List<KlassAine>>(File.ReadAllText(ainedFilename));
        }

        public override string ToString() => $"ainet \"{Aine}\" õpetatakse {Klass} klassis";
    }

    public class Hinne
    {
        static string folder = @"..\..\..\";

        public static List<Hinne> Hinded = new List<Hinne>();

        public string ÕpetajaIK { get; set; }
        public string ÕpilaseIK { get; set; }
        public string Aine { get; set; }
        public int Number { get; set; }

        public Hinne() => Hinded.Add(this);

        public static void LoeHinded()
        {
            string hindedFilename = folder + "hinded.txt";

            File.ReadAllLines(hindedFilename)
                .Select(x => (x + ",,").Split(',').Select(y => y.Trim()).ToArray())
                .ForEach(x => new Hinne { ÕpetajaIK = x[0], ÕpilaseIK = x[1], Aine = x[2], Number = int.Parse(x[3]) });

        }

        public static void KirjutaHindedJson()
        {
            string hindedFilename = folder + "hinded.json";

            File.WriteAllText(hindedFilename,
                JsonConvert.SerializeObject(Hinded)
                );
        }

        public static void LoeHindedJson()
        {
            string hindedFilename = folder + "hinded.json";

            Hinded = 
            JsonConvert.DeserializeObject<List<Hinne>>(
            File.ReadAllText(hindedFilename));
        }


        public override string ToString() => $"{Inimene.ByIK(ÕpilaseIK)?.Nimi ?? "nimetu"} sai aines {Aine} hindeks {Number} (õpetaja {Inimene.ByIK(ÕpetajaIK)?.Nimi ?? "nimetu"})";
    }
}
