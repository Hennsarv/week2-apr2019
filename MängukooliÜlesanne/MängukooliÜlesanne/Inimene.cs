﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Newtonsoft.Json;

namespace MängukooliÜlesanne
{
    public class Inimene
    {
        static string folder = @"..\..\..\";


        static Dictionary<string, Inimene> _Inimesed = new Dictionary<string, Inimene>();
        public static IEnumerable<Inimene> Inimesed => _Inimesed.Values;

        public string IK { get; set; }  // pidin private seti maha võtma - muidu ei saa deserialiseerida
        public string Nimi { get; set; }
        public string Klass { get; set; } = ""; // õpilastel, kus õpib, õpetajatel, mida juhatab
        public string Aine { get; set; } = ""; // õpilastel puudub  // pidin privati är anoppima

        public static IEnumerable<Inimene> Õpilased => Inimesed.Where(x => x.Aine == "");
        public static IEnumerable<Inimene> Õpetajad => Inimesed.Where(x => x.Aine != "");

        public Inimene() { } // pidin lubama parameetriteta public construktori, muidu ei saa deserialiseerida
        Inimene(string ik) => _Inimesed.Add(IK = ik, this);

        internal static void LoeÕpetajad()
        {
            string õpetajadFilename = folder + "õpetajad.txt";
            

            File.ReadAllLines(õpetajadFilename)
                .Select(x => (x + ",,").Split(',').Select(y => y.Trim()).ToArray())
                .ForEach(x => Inimene.NewÕpetaja(x[0], x[1], x[2], x[3]));
        }
        internal static void LoeÕpilased()
        {
            string õpilasedFilename = folder + "õpilased.txt";
            File.ReadAllLines(õpilasedFilename)
                .Select(x => (x + ",,").Split(',').Select(y => y.Trim()).ToArray())
                .ForEach(x => Inimene.NewÕpilane(x[0], x[1], x[2]));
        }

        public static void KirjutaInimesedJson()
        {
            string inimesedFilename = folder + "inimesed.json";

            File.WriteAllText(inimesedFilename,
                JsonConvert.SerializeObject(Inimesed)
                );
        }

        public static void LoeInimesedJson()
        {
            string inimesedFilename = folder + "inimesed.json";

            var ajuti  = JsonConvert.DeserializeObject<Inimene[]>(
                File.ReadAllText(inimesedFilename)
                )
                ;

            _Inimesed = ajuti.ToDictionary(x => x.IK);



        }
        //{
        //    IK = ik;
        //    _Inimesed.Add(ik, this);
        //}

        public static Inimene NewÕpilane(string ik, string nimi, string klass)
            => _Inimesed.ContainsKey(ik)
            ? null
            : new Inimene(ik) { Nimi = nimi, Klass = klass };

        public static Inimene NewÕpetaja(string ik, string nimi, string aine, string klass = "")
                       => _Inimesed.ContainsKey(ik)
            ? null
            : new Inimene(ik) { Nimi = nimi, Aine = aine, Klass = klass };

        public override string ToString()
            => (Aine == ""
            ? $"{Klass} klassi õpilane"
            : $"{Aine} õpetaja")
            + $" {Nimi}" +
            (Aine != "" && Klass != "" ? $" ({Klass} klassi juhataja)" : "");

        public static Inimene ByIK(string ik)
            => _Inimesed.ContainsKey(ik) ? _Inimesed[ik] : null;



    }

}
