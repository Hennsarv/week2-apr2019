﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace MängukooliÜlesanne
{
    static class Program
    {
        internal static void ForEach<T>(this IEnumerable<T> collection, Action<T> action)
        {
            foreach (T x in collection) action(x);
        }
       
        static void Main(string[] args)
        {


            // failide nimed


            // loen sisse failid (ja teisendan kohe objektideks)
            Console.Write("Kas loen jsonist (jah, ei): ");
            if (Console.ReadLine() == "jah")
            {
                Inimene.LoeInimesedJson();
                Hinne.LoeHindedJson();
                KlassAine.LoeKlassidAined();
            }
            else
            {
                Inimene.LoeÕpetajad();
                Inimene.LoeÕpilased();
                KlassAine.LoeKlassidAined();
                Hinne.LoeHinded();
            }
            Console.Write("Kas kirjutan jsonid (jah, ei): ");
            if (Console.ReadLine() == "jah")
            {
                Inimene.KirjutaInimesedJson();
                Hinne.KirjutaHindedJson();
                KlassAine.KirjutaKlassidAinedJson();
            }

            Console.WriteLine("lugesin andmed sisse, kas tahad üle vaadata (jah, ei)? ");

            if (Console.ReadLine() == "jah")
            {

                //Inimene.Inimesed.ForEach(x => Console.WriteLine(x)); // prindin välja õpetajad ja õpilased

                Console.WriteLine("\nÕpetajad:");
                foreach (var x in Inimene.Õpetajad) Console.WriteLine(x);
                Console.WriteLine("\nÕpilased:");
                foreach (var x in Inimene.Õpilased) Console.WriteLine(x);

                Console.WriteLine("\nKlassi ja ained:");

                foreach (var x in KlassAine.KlassAined) Console.WriteLine(x); // prindin välja klassid-ained

                Console.WriteLine("\nHinded:");

                foreach (var x in Hinne.Hinded) Console.WriteLine(x); // prindin välja hinded

            }
            if (false)
            {
                // siin kohal on andmed sisse loetud
                // lubasin teile rääkida, mis asi on see ?

                // siit mõned näited nullable tüübi kasutamisest
                int? palk = 10000;
                palk = null;
                Console.WriteLine($"palk on {palk / 12}");

                Palk? p = new Palk { põhipalk = 7000, lisatasu = 3000 };
                p = null;

                Hinne.Hinded
                    .Where(x => x.Aine == "füüsika")
                    .ForEach(x => Console.WriteLine(x));

                Console.WriteLine(Hinne.Hinded.Where(x => x.Aine == "matemaatika").FirstOrDefault().Number);

                int?[] arvud = { 1, 2, 3, 4, 5, 9, 7 };
                Console.WriteLine(arvud.Where(x => x > 5).FirstOrDefault() ?? -1);
            }

            while (true)
            {
                Console.Write("mida ssa uurida tahad (õpilased (1), õpetajad (2), klassid (3), lõpetamiseks (0): ");
                switch (Console.ReadLine())
                {
                    case "0":
                    case "": goto Edasi;
                    // tahan tsüklist välja, aga break ei aita, kuna break viib meid SWITCHIST välja
                    case "1":
                        // siia õpilaste andmed
                        ÕpilasteAndmed();
                        break;
                    case "2":
                        // siia õpetajate andmed
                        ÕpetajateAndmed();
                        break;
                    case "3":
                        // siia klasside andmed
                        KlassideAndmed();
                        break;
                    default:
                        Console.WriteLine("tee korralik valik");
                        break;
                }

                }
                Edasi:  // nii on võimlaik märkida koht, kuhu maabuda tsüklist välja
                ;
            }

        private static void ÕpilasteAndmed()
        {
            List<Inimene> õpilased = new List<Inimene>();
            while (õpilased.Count == 0)
            {
                Console.Write("ütle õpilase nimi: ");
                var nimi = Console.ReadLine();
                if (nimi == "") return;
                õpilased = Inimene.Õpilased.Where(x => x.Nimi.ToLower().Contains(nimi.ToLower())).ToList();
                if (õpilased.Count == 0) Console.WriteLine("sellise nimega meil kedagi pole");
            }
            Inimene õpilane = null;

            if (õpilased.Count > 1)
            {
                for (int i = 0; i < õpilased.Count; i++)
                {
                    Console.WriteLine($"{i + 1}. {õpilased[i]}");
                }
                int mitmes = -1;
                while (true)
                {
                    Console.Write("kelle andmeid sa vajad (0 - tagasi)");
                    if (int.TryParse(Console.ReadLine(), out mitmes))
                    {
                        if (mitmes == 0) return;
                        if (mitmes <= õpilased.Count) break;
                        Console.WriteLine("sellise numbriga pole ");
                    }
                    else Console.WriteLine("vigane nr");
                }
                õpilane = õpilased[mitmes - 1];

            }
            else õpilane = õpilased[0];

            Console.WriteLine(õpilane);
            Console.WriteLine("Tema keskmised hinded ainete kaupa:");
            Hinne.Hinded.Where(x => õpilane.IK == x.ÕpilaseIK)
                .ToLookup(x => x.Aine)
                .Select(x => new { Aine = x.Key, Keskmine = x.Average(y => y.Number) })
                .ForEach(x => Console.WriteLine($"\tAines {x.Aine} - {x.Keskmine}"));

        }

        static void ÕpetajateAndmed()
        {

        }

        static void KlassideAndmed()
        {
            var klassõpilased = Inimene.Õpilased.ToLookup(x => x.Klass);
            Console.WriteLine("meil on sellised klassid:");
            klassõpilased.ForEach(x => Console.Write($"{x.Key} "));
            Console.WriteLine();
            Console.Write("milline sind huvitab: ");
            var klass = Console.ReadLine();
            if (klass == "") return;

            if (klassõpilased.Where(x => x.Key == klass).Count() > 0)
            {
                Console.Write("kas klassi nimekiri (1), hinded (2) või ained (3): ");
                switch (Console.ReadLine())
                {
                    case "1":
                        Console.WriteLine("selles klassis õpivad: ");
                        foreach (var x in klassõpilased[klass]) Console.WriteLine(x);
                        break;
                    case "2":
                        Console.WriteLine("õpilaste keskmised hinded:");
                        klassõpilased[klass]
                            .Select(x => new {
                                x.Nimi,
                                Keskmine = Hinne.Hinded
                                            .Where(y => y.ÕpilaseIK == x.IK)
                                            .DefaultIfEmpty()
                                            .Average(y => y?.Number ?? null)
                            })
                            .ForEach(x => Console.WriteLine($"{x.Nimi} keskmine hinne on {x.Keskmine?.ToString() ?? "pole hindeid"}"));
                        break;
                    case "3":
                        Console.WriteLine("selles klassis õpetatakse järgmisi asju: ");
                        foreach (var x in KlassAine.KlassAined.Where(x => x.Klass == klass))
                        {
                            Console.WriteLine(x.Aine);
                        }
                        break;
                }
            }
            else Console.WriteLine("sellist klassi meil pole");

        }


    }

    struct Palk
        {
            public int põhipalk;
            public int lisatasu;
            public int kokku => põhipalk + lisatasu;
            public override string ToString() => $"põhi={põhipalk} lisa={lisatasu}";
        }

        
       
    }
