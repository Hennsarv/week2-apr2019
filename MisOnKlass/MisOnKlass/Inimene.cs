﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MisOnKlass
{
    class Inimene
    {
        // väljad - muutujad, mis sisaldavad seda klassi objekti tükke (väärtusi)
        public string EesNimi;
        public string PereNimi;
        public int Vanus;

        public override string ToString() => $"{TäisNimi("f")} vanusega {Vanus}";

        // funktsioon - arvutus, mida saab teha selle klassi väljadega
        public string TäisNimi(string kuju)
        {
            return 
                kuju == "s" ? EesNimi.Substring(0,1) + ". " + PereNimi :
                EesNimi + " " + PereNimi;
        }

        // meetod - tegevus, mida saab teha selle klassi objektidega
        public void Tere()
        {
            Console.WriteLine($"Tere {TäisNimi("f")}!");
        }

    }
}
