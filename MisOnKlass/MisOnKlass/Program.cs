﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MisOnKlass
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Tere Inimene!");

            Inimene henn = new Inimene { EesNimi = "Henn", PereNimi = "Sarv", Vanus = 64 };
            Inimene ants = new Inimene();  //{ EesNimi = "Ants", PereNimi= "Saunamees", Vanus = 28 };
            ants.EesNimi = "Ants";
            ants.PereNimi = "Saunamees";
            ants.Vanus = 28;
            ants.Tere();
            string antsuNimi = ants.TäisNimi("s");

           // Console.WriteLine(henn);
           // Console.WriteLine(ants);

            List<Inimene> inimesed = new List<Inimene>();
            inimesed.Add(henn);
            inimesed.Add(ants);
            inimesed.Add(new Inimene { EesNimi = "Peeter", PereNimi = "Suur", Vanus = 40 });

            string nimekiri = "";
            foreach (var x in inimesed) nimekiri += x.TäisNimi("s") + " ";
            Console.WriteLine($"nimekiri: {nimekiri}");
            foreach (var x in inimesed) x.Tere(); 
        }
    }





}
