﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mängukool
{
    class Inimene
    {
        // inimene klassi static asjad - dictionary ja selle najal nimekiri
        protected static Dictionary<string, Inimene> _Inimesed = new Dictionary<string, Inimene>();
        public static IEnumerable<Inimene> Inimesed => _Inimesed.Values;

        // millest koosneb inimene - nimest ja isikukoodist
        public string Nimi { get; set; }
        public string IK { get; private set; }

        // sisemine konstruktor, mis inimese valmis teeb ja sõnastikku paneb
        protected Inimene(string ik)
        {
            IK = ik;
            _Inimesed.Add(ik, this);
        }

        // static factory meetod inimese tegemiseks (kui korduv isikukood, siis ei tee)
        public static Inimene New(string ik, string nimi)
            =>_Inimesed.ContainsKey(ik) ? null : new Inimene(ik) { Nimi = nimi };

        // inimene stringiks (lihtne variant)
        public override string ToString() => $"{Nimi} (IK: {IK})";

    }

    // sealed tähendab, et edasi tuletada ei saa
    // kui oleks vaja, tuleks ka konstruktor teha protected
    sealed class Õpilane : Inimene
    {
        // õpilane lisaks  inimeseks olemisele õpib mingis klassis
        public string Klass { get; set; } // klass, kus õpib

        // privaatne (või protected) konstruktor
        // privaatne konstruktor teeb võimatuks klassi tuletamise Õpilasest (vt ka sealed)
        private Õpilane(string ik) : base(ik) { }

        // õpilase on oma factory
        public static Õpilane New(string ik, string nimi, string klass)
            => _Inimesed.ContainsKey(ik) ? null : new Õpilane(ik) { Nimi = nimi, Klass = klass };

        // ja piut keerukam tostring
        public override string ToString()
        =>  $"{Klass} klassi õpilane "
            + base.ToString(); // kasutame ära baasklassi tostringi
        
    }
    sealed class Õpetaja : Inimene
    {
        public string Aine { get; set; } // aine, mida õpetab
        public string Klass { get; set; } = ""; // klass, mida juhatab 
        private Õpetaja(string ik) : base(ik) { }

        public static Õpetaja New(string ik, string nimi, string aine, string klass = "")
            => _Inimesed.ContainsKey(ik) ? null : new Õpetaja(ik) { Nimi = nimi, Aine = aine, Klass = klass };

        public override string ToString()
        => $"{Aine} õpetaja "
            + (Klass == "" ? "" : $"ja {Klass} klassi juhataja ")
            + base.ToString();


    }
}
