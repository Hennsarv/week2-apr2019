﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Mängukool
{
    class Program
    {
        static void Main(string[] args)
        {
            string folder = @"..\..\";
            string õpilasedFile = folder +"õpilased.txt";
            string õpetajadFile = folder +"õpetajad.txt";

            var õpilased = File.ReadAllLines(õpilasedFile);
            var õpetajad = File.ReadAllLines(õpetajadFile);

            Inimene.New("35503070211", "Henn Sarv");
            Õpilane.New("35503070212", "Henn Sarv", "1A");
            Õpetaja.New("35503070213", "Henn Sarv", "Matemaatika", "2B");

            foreach (var rida in õpilased)
            {
                var osad = rida.Split(',');
                Õpilane.New(osad[0].Trim(), osad[1].Trim(), osad[2].Trim());
            }

            foreach (var rida in õpetajad)
            {
                var osad = (rida+"," ).Split(','); // see + "," tagab, et reamassiivid on "ühepikkused"
                Õpetaja.New(osad[0].Trim(), osad[1].Trim(), osad[2].Trim(), osad[3].Trim());
            }



            foreach (var x in Inimene.Inimesed) Console.WriteLine(x);


        }
    }


}
