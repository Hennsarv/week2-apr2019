﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Anonymous
{
    static class Program
    {

        public static void ForEach<T>(this IEnumerable<T> m, Action<T> a)
        {
            foreach (var x in m) a(x);
        }

        static void Main(string[] args)
        {
            var misseeon = new { Nimi = "Henn", Vanus = 64 };

            Console.WriteLine(misseeon.GetType().Name);

            Console.WriteLine(misseeon);

            int[] arvud = { 1, 2, 3, 4, 5 };

            var ruudud = arvud.Select(x => new { Arv = x, Ruut = x * x });

            foreach (var x in ruudud) Console.WriteLine(x);

            var read = File.ReadAllLines(@"..\..\spordipäeva protokoll.txt");

            var read2 = read
                .Skip(1)
                .Select(x => x.Split(','))
                .Select(x => new { Nimi = x[0].Trim(), Distants = int.Parse(x[1].Trim()), Aeg = int.Parse(x[2].Trim()) })
                .Select( x => new { x.Nimi, x.Aeg, x.Distants, Kiirus = x.Distants * 1.0 / x.Aeg})
                ;

//            foreach (var x in read2) Console.WriteLine(x);

            read2.ForEach(x => Console.WriteLine(x));

            

            //Console.WriteLine(read2.Min(x => x.Distants * 1.0 / x.Aeg));

            //Console.WriteLine("kõige kiirem on " +
            //    read2
            //    .OrderByDescending(x => x.Kiirus)
            //    .First().Nimi);

            //int minDistantce = read2.Min(y => y.Distants);
            //var q1 = (from x in read2
            //          where x.Distants == minDistantce
            //          orderby x.Kiirus descending
            //          select new { x.Nimi, x.Kiirus })
            //        .ToList()
                    
            //        ;

            //q1.ForEach(x => Console.WriteLine(x));

            //var q2 = read2
            //    .Where(x => x.Distants == 100)
            //    .OrderByDescending(x => x.Kiirus)
            //    .Select(x => new { x.Nimi, x.Kiirus })
            //    .ToList()
            //    ;

            ////foreach (var x in q2) Console.WriteLine(x);
            //Console.WriteLine( q2.First()  );


            // kuidas ma leiaksin kõige stabiilsema jooksja
            // vähemalt 2 distantsi jooksnud
            // kiiruste vahe on minimaalne

            // TODO: Esmaspäeval vaatame üle


            Console.WriteLine("\n kõige stabiilsem \n");

            Console.WriteLine
            (read2.ToLookup(x => x.Nimi)
                .Where(x => x.Count() > 1)
                .Select(x => new { Nimi = x.Key, Vahe = (x.Max(y => y.Kiirus) - x.Min(y => y.Kiirus)) })
                .OrderBy(x => x.Vahe)
                .FirstOrDefault()?.Nimi ?? "sellist pole");

            var tulemused = read2.ToLookup(x => x.Nimi.ToLower());

            foreach (var x in tulemused) Console.WriteLine(x.Key);

            while(true)
            {
                Console.Write("kelle tulemusi tahad: ");
                var kes = Console.ReadLine().ToLower();
                if (kes == "") break;

                foreach (var x in tulemused[kes]) Console.WriteLine(x);

            }


        }
    }
}
