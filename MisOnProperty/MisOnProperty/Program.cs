﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MisOnProperty
{
    class Program
    {
        static void Main(string[] args)
        {
            Inimene henn = new Inimene { EesNimi = "Henn", PereNimi= "Sarv", Vanus = 64 };
            //henn.setVanus(64);
            //henn.setVanus(65);
            //Console.WriteLine(henn.getVanus());

            
            henn.Vanus++;
            //henn.setVanus(henn.getVanus() + 1); // nii kirjutad Javas

            Console.WriteLine(henn.Vanus);
        }
    }

    class Inimene
    {
        public string EesNimi;
        public string PereNimi; 
        private int _Vanus;


        // sellist oleks sul vaja Javas
        public void setVanus(int uusVanus)
        {
            if (uusVanus > _Vanus) _Vanus = uusVanus;
        }

        // sellist oleks ka sul vaja Javas
        //public int getVanus() { return _Vanus; }
        public int getVanus() => _Vanus;

        // tavaline property get ja set osaga
        public int Vanus
        {
            //get { return _Vanus; }
            //set { _Vanus = value > _Vanus ? _Vanus = value : _Vanus; }
            get => _Vanus; 
            set => _Vanus = value > _Vanus ? _Vanus = value : _Vanus; 
        }

        // readonly property pikalt
        public string Täisnimi
        {
            get { return $"{EesNimi} {PereNimi}"; }
            // set-i kohta ei oska ma midagi kosta
        }

        // samasugune readonly property lühidalt
        public string TäisNimi => $"{EesNimi} {PereNimi}";

        // fünktsioon lühidalt
        public override string ToString() => $"inimene {TäisNimi} on {Vanus} aastane";

    }
}
