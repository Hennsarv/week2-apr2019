﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Aken1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Inimene.Inimene i = new Inimene.Inimene
            {
                Nimi = this.textBox1.Text,
                Vanus = int.Parse(this.textBox2.Text)
            };
            this.label1.Text = i.ToString();
        }
    }
}
