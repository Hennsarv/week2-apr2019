﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inimene
{
    public class Inimene
    {
        public string Nimi;
        public int Vanus;

        public override string ToString() => $"{Nimi} {Vanus} aastane";

        public Inimene()
        {
            this.Vanus = 7;
        }
        
    }
}
