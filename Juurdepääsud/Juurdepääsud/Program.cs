﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Juurdepääsud
{
    enum Gender { Naine, Mees}

    struct Nimi
    {
        public string Eesnimi;
        public string Perenimi;

        public override string ToString() => $"{Eesnimi} {Perenimi}".Trim();
        
    }

    class Koer
    {
        string Nimi;
        private int jalgadeArv = 4;   // vaikimisi on privaatne

        public int JalgadeArv => jalgadeArv;
    }

    static class Program
    {
        static int klassiOma = 0;
        static int imelik = 17;

        static void Main(string[] args)
        {

            Nimi n = new Nimi { Eesnimi = "Henn", Perenimi = "Sarv" };
            Nimi n1 = n;
            n1.Eesnimi = "Kalle";
            Console.WriteLine(n);

            

            int sisemine = klassiOma + 2;
            Console.WriteLine(sisemine);

            {
                int veelsisemisem = sisemine * 8;
                string klassiOma = "loll";

            }

            Koer k = new Koer();

            Console.WriteLine(k.JalgadeArv);

            Klassid.Inimene i = new Klassid.Inimene { Nimi = "Henn" };
            

            Kass kass = new Kass();
        }

        static void Segane()
        {
            imelik = 7;
        }

        static void Teine()
        {
            klassiOma = 8;
            
        }
    }
}
