﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KlassidEdasi
{
    class Person
    {
        static int loendur = 0;  // staatiline väli - see on klassil mitte objektil
        public static int Count => loendur;  // staatiline property - readonly

        private static Dictionary<string, Person> ListOfPeople = new Dictionary<string, Person>();

        public static IEnumerable<Person> People => ListOfPeople.Values;

        //public static List<Person> People = new List<Person>(); // staatiline nimekiri

        private string _FirstName;      // siit alates on mittestaatilised väljad - objekti väljad
        private string _LastName;
        public string PID { get; private set; }      // syymmddnnnx  s - sugu ja sajand  .Substring(0,1)
        // privately settable property

        // variant 1 - private väli ja readonly property
        // private int _Nr  = ++loendur;
        // public int Nr => _Nr;

        // variant 2 - readonly public väli
        // public readonly int Nr = ++loendur;    // readonly public field

        // variant 3 - public readonly property
        public int Nr { get; } = ++loendur;

        // see on täitsa uus asi - konstruktor // sellest räägime natuke
        private Person(string pid) : this(pid, "")
        {
            //PID = pid;
            //People.Add(this);
        }

        // overloaditud konstruktor
        private Person(string pid, string name) //: this(pid)
        {

            PID = pid;
            //People.Add(this);
            ListOfPeople.Add(pid, this);

            string[] names = (name).Split(' ');
            FirstName = names[0];
            if (names.Length > 1)
                LastName = names[1];
        }

        public static Person Create(string pid, string name)
        {
            if (ListOfPeople.ContainsKey(pid)) return null;
            return new Person(pid, name);
        }

    

        #region Hiljem vaatame, mis vanemate ja lastega teha
        //public int ShoeNumber; // default väärtus on 0   zero 
        //public Person Mother; // default väärtus on null null
        //public Person Father;

        //public List<Person> Children = new List<Person>(); 
        #endregion

        public Gender Gender => (Gender)(PID[0] % 2);

        public String FullName => $"{FirstName} {LastName}".Trim();  // siia mõtle miskit välja

        public DateTime DayOfBirth // kiirem ja lihtsam aga keerulisem kirjutada
        => new DateTime(
                ((PID[0] - '1') / 2 + 18) * 100 +
                int.Parse(PID.Substring(1, 2)),
                int.Parse(PID.Substring(3, 2)),
                int.Parse(PID.Substring(5, 2))
                );

        public override string ToString() => $"{Nr}. {Gender}: {FullName} ({PID})";

        // kaks asja juurde
        // public int Age - mis annaks vanuse aastates (täisaastates)
        // tehke eesnimi (Firstname) ja perenimi (LastName) niimoodi ringi, et oleks täidetud reegel
        // nimi peab algama suure tähega ja ülejäänud tähed on väikesed

        public int Age // kui panna Age taha sulud Age() siis on funktsioon, muidu r/o property
            => (DateTime.Today - DayOfBirth).Days * 4 / 1461; // siia nulli asemele siis avaldis, mis annaks vanuse

        public string FirstName
        {
            get => _FirstName;
            set => _FirstName = value.ToProper().Trim(); // siia siis miski, mis tagaks, et eesnimi alati suure tähega
        }

        public string LastName
        {
            get => _LastName;
            set => _LastName = value.ToProper().Trim(); // siia siis miski, mis tagaks, et eesnimi alati suure tähega
        }

        public static bool TryParse(string tekst, out Person person)
        {
            person = null;

            var osad = tekst.Split(' ');
            if (osad.Length != 3) return false;
            if (ListOfPeople.ContainsKey(osad[0])) return false;
            person = new Person(osad[0]) { FirstName = osad[1], LastName = osad[2] };
            return true;
        }



    }
}
