﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KlassidEdasi
{
    static class E
    {
        public static string ToProper(this string s)
            => s == "" ? "" :
            s.Substring(0, 1).ToUpper() + s.Substring(1).ToLower();
    }

    class Program
    {
        static void Main(string[] args)
        {
            

            Person p = Person.Create("35503070211", "henn sarv" );
           
            //Console.WriteLine(p);
            Person.Create("45504044444", "Malle mallikas" );

            if (Person.Create("45504044445", "Ülle Ülane") == null) Console.WriteLine("selline juba on"); 
            
            //Console.WriteLine(p);

            Person.Create("50505050505", "milli mallikas");
            Person.Create("50707070707", "donald");

            Console.Write("anna uus inimene (isikukood ja nimed tühik vahele): ");

            //new Person(uus[0]) { FirstName = uus[1], LastName = uus[2] };
            if (Person.TryParse(Console.ReadLine(), out p))
                Console.WriteLine("olemas");
            else Console.WriteLine("ei saa");


            Console.WriteLine($"inimesi on meil {Person.Count}");

            foreach (var x in Person.People) Console.WriteLine(x);

            if (Person.TryParse("18901010000 Hannes Saarepuu", out Person pp))
            {
                Console.WriteLine(pp);
            }
            else Console.WriteLine("seda inimest ei saa teha");

        }
    }
}
