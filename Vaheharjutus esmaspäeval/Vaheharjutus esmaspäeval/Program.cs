﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vaheharjutus
{
    class Program
    {
        static void Main(string[] args)
        {
            Person mina = new Person { FirstName = "Henn", LastName = "Sarv", PID = "35503070211" };
            Console.WriteLine(mina.FullName); // peaks tulema Henn Sarv
            Console.WriteLine(mina.DayOfBirth); // peaks tulema 7. märts 1955
            Console.WriteLine(mina.Gender); // peaks tulema Male
        }
    }

    public enum Gender { Female, Male}

    class Person
    {
        public string FirstName;
        public string LastName;
        public string PID;      // syymmddnnnx  s - sugu ja sajand  .Substring(0,1)
                                //              yy - aasta          .Substring(1,2)
                                //              mm - kuu            .Substring(3,2)
                                //              dd - päev           .Substring(5,2)

        public Gender GetGender() // selge ja arusaadav
        {  
            switch (PID.Substring(0,1))
            {
                case "1": case "3": case "5":
                    return Gender.Male;
                default:
                    return Gender.Female;
            }
        }
        
        public Gender Gender // kiire ja elegantne aga raske lugeda ja aru saada
        => (Gender)(PID[0] % 2);
        

        public String FullName => $"{FirstName} {LastName}";  // siia mõtle miskit välja

        public DateTime DayOfBirth // selge ja arusaadav aga pikk kurat
        {
            get
            {

                int aasta = int.Parse(PID.Substring(1, 2));
                switch (PID.Substring(0, 1))
                {
                    case "1": case "2": aasta += 1800; break;
                    case "3": case "4": aasta += 1900; break;
                    case "5": case "6": aasta += 2000; break;
                }
                int kuu = int.Parse(PID.Substring(3, 2));
                int päev = int.Parse(PID.Substring(5, 2));
                return new DateTime(aasta, kuu, päev);
            }
        }// siia mõtle miskit välja

        public DateTime DayOfBirth2 // kiirem ja lihtsam aga keerulisem kirjutada
        => new DateTime(
                ((PID[0]-'1')/2+18)*100+
                int.Parse(PID.Substring(1, 2)),
                int.Parse(PID.Substring(3, 2)),
                int.Parse(PID.Substring(5, 2))
                );
        
    }


    class Inimene
    {
        public string Nimi { get; set; }
        public int Vanus { get; private set; }
    }

}
